[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/folder-watcher)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c7de4476771c4c6384ad36e2b2ddcac2)](https://www.codacy.com/bb/wirbelwild/folder-watcher/dashboard)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/folder-watcher/v/stable)](https://packagist.org/packages/bitandblack/folder-watcher)
[![Total Downloads](https://poser.pugx.org/bitandblack/folder-watcher/downloads)](https://packagist.org/packages/bitandblack/folder-watcher)
[![License](https://poser.pugx.org/bitandblack/folder-watcher/license)](https://packagist.org/packages/bitandblack/folder-watcher)

# Folder Watcher

Watches a folder and handles its files. This script is meant to run from your CLI. 

It may be used to realize a hotfolder on your local system.

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/folder-watcher). Add it to your project by running `$ composer require bitandblack/folder-watcher`.

## Usage 

Create a new object and tell it the path to the folder you want to watch and a pattern for the files you want to select:

````php
<?php

use BitAndBlack\FolderWatcher\FolderWatcher;

$watcher = new FolderWatcher(
    '/path/to/your/folder',
    '*.txt'
);
````

Set a callback function to handle the files one after another:

````php
<?php

$watcher->setCallback(static function($file) {    
    echo 'Handling file ' . $file . ' now' . PHP_EOL;
});
````

Start the watcher: 

````php
<?php

$watcher->watch();
````

### Cancel the watcher

Per default the watcher is meant to run forever. You can change this by calling `finishOnEmptyFolder()`. It's always possible to abort by pressing the keys `Ctrl` + `C`. The script will stop after finishing the current task.

### Logging 

The watcher may give information about the files inside a folder and is able to pass it to the Symfony console. Set the console output object to the watcher with `setOutput($consoleOutput)`.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).