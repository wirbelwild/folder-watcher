# Changes in Bit&Black Folder Watcher

## 1.1.0 2023-01-13

### Changed

-   Improved how to abort the script. The keys `Q` + `Enter` are no longer working: `Ctrl` + `C` is the only possibility now.

## 1.0.0 2022-11-19

### Changed

-   PHP >=7.4 is now required.

### Added

-   Added third parameter to `FolderWatcher::__construct` to define, if subfolders should be included too.