<?php

/**
 * Folder Watcher
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FolderWatcher\Tests;

use BitAndBlack\FolderWatcher\Exception\PathNotReadableException;
use BitAndBlack\FolderWatcher\FolderWatcher;
use PHPUnit\Framework\TestCase;

/**
 * Class FolderWatcherTest
 *
 * @package BitAndBlack\FolderWatcher\Tests
 */
class FolderWatcherTest extends TestCase
{
    public function testCanInitExistingPath(): void
    {
        new FolderWatcher(
            __DIR__,
            '*'
        );
        
        self::assertTrue(true);
    }
    
    public function testThrowsPathNotReadableException(): void
    {
        $this->expectException(PathNotReadableException::class);

        new FolderWatcher(
            'MISSING_PATH',
            '*'
        );
    }
}
