<?php

/**
 * Folder Watcher
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FolderWatcher;

/**
 * Class Exception
 *
 * @package BitAndBlack\FolderWatcher
 */
class Exception extends \Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
