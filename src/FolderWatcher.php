<?php

/**
 * Folder Watcher
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FolderWatcher;

use BitAndBlack\FolderWatcher\Exception\PathNotReadableException;
use BitAndBlack\Helpers\ArrayHelper;
use BitAndBlack\Measurement\Exception\TimeException;
use BitAndBlack\Measurement\Measurement;
use BitAndBlack\Measurement\Unit;
use BitAndBlack\PathInfo\PathInfo;
use Closure;
use Khill\Duration\Duration;
use React\EventLoop\Loop;
use React\EventLoop\LoopInterface;
use ReactFileWatcher\Exceptions\PathDoesntExist;
use ReactFileWatcher\FileWatcherFactory;
use ReactFileWatcher\PathObjects\PathWatcher;
use Spatie\Emoji\Emoji;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FolderWatcher
 *
 * @package BitAndBlack
 */
class FolderWatcher
{
    /**
     * @var array<int, string>
     */
    private array $fileQueue = [];

    private bool $finishOnEmptyFolder = false;

    private int $timeOut = 1;

    /**
     * @var callable
     */
    private $callback;

    private string $path;

    private string $filesPattern;

    private OutputInterface $output;

    private bool $includeSubfolders;

    private Measurement $measurement;

    private ?Unit $unit = null;

    private int $handledFilesCount = 0;

    private bool $hasStopped = false;

    /**
     * FolderWatcher constructor.
     *
     * @param string $path            Path to the folder, that should be watched.
     * @param string $filesPattern    The pattern to find the files, for example `*.txt`.
     * @param bool $includeSubfolders If files in subfolders should be handled or not.
     * @throws PathNotReadableException
     */
    public function __construct(string $path, string $filesPattern = '*.*', bool $includeSubfolders = false)
    {
        $this->path = rtrim($path, DIRECTORY_SEPARATOR);

        if (!is_dir($this->path)) {
            throw new PathNotReadableException($this->path);
        }

        $this->filesPattern = $filesPattern;
        $this->output = new NullOutput();
        $this->callback = static function () {
        };
        $this->includeSubfolders = $includeSubfolders;
        $this->measurement = new Measurement();
    }

    /**
     * @param OutputInterface $output
     * @return FolderWatcher
     */
    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @param callable $callback
     * @return FolderWatcher
     */
    public function setCallback(callable $callback): self
    {
        $this->callback = $callback;
        return $this;
    }

    /**
     * Starts watching a folder.
     *
     * @return void
     * @throws PathNotReadableException
     */
    public function watch(): void
    {
        $this->unit = new Unit('Folder watcher');

        try {
            $this->measurement->add(
                $this->unit->start()
            );
        } catch (TimeException $timeException) {
            $this->output->writeln(Emoji::crossMark() . ' Failed starting times.');
        }

        $this->output->writeln(Emoji::eyes() . ' Watching folder "<comment>' . $this->path . '</comment>". Press <comment>Ctrl</comment> + <comment>C</comment> to abort.');

        /** @var array<int, string> $filesExisting */
        $filesExisting = ArrayHelper::getArray(
            glob($this->path . DIRECTORY_SEPARATOR . $this->filesPattern)
        );

        foreach ($filesExisting as $file) {
            $this->addFileToQueue($file);
        }

        try {
            $pathWatcher = new PathWatcher($this->path);
        } catch (PathDoesntExist $exception) {
            throw new PathNotReadableException($this->path);
        }

        $loop = Loop::get();
        $fileWatcher = FileWatcherFactory::create($loop);

        $displayedWaitMessage = false;

        $loop->addPeriodicTimer(
            $this->timeOut,
            $this->handlePeriodicTimer($loop, $displayedWaitMessage)
        );

        if (defined('SIGINT')) {
            $loop->addSignal(SIGINT, function () use ($loop) {
                $this->hasStopped = true;
                $loop->stop();
                $this->output->writeln('');
                $this->showStatistics();
            });
        }

        $fileWatcher->Watch(
            [$pathWatcher],
            $this->handleWatch()
        );

        $loop->run();
    }

    /**
     * Sets how ofter the callback should be fired.
     *
     * @param int $timeOut Time in seconds.
     * @return FolderWatcher
     */
    public function setTimeOut(int $timeOut): self
    {
        $this->timeOut = $timeOut;
        return $this;
    }

    /**
     * Allows the watcher to finish when the folder is empty.
     *
     * @return FolderWatcher
     */
    public function finishOnEmptyFolder(): self
    {
        $this->finishOnEmptyFolder = true;
        return $this;
    }

    /**
     * @param string $file
     * @return void
     */
    private function handleFile(string $file): void
    {
        $this->output->write(Emoji::gear() . ' Handling file "<comment>' . basename($file) . '</comment>"... ');

        $callback = $this->callback;
        $callback($file);

        $this->output->writeln('Done.');

        unlink($file);

        if (false !== $key = array_search($file, $this->fileQueue, true)) {
            unset($this->fileQueue[$key]);
        }

        ++$this->handledFilesCount;
    }

    /**
     * @param string $file
     */
    private function addFileToQueue(string $file): void
    {
        $hasBeenDeleted = !file_exists($file);

        if ($hasBeenDeleted) {
            if (false !== $key = array_search($file, $this->fileQueue, true)) {
                unset($this->fileQueue[$key]);
            }

            return;
        }

        $this->fileQueue[] = $file;
        $this->output->writeln(Emoji::plus() . '  Detected file "<comment>' . basename($file) . '</comment>".');
    }

    /**
     * @param LoopInterface $loop
     * @param bool $displayedWaitMessage
     * @return Closure
     */
    private function handlePeriodicTimer(LoopInterface $loop, bool &$displayedWaitMessage): Closure
    {
        return function () use ($loop, &$displayedWaitMessage) {
            if (0 === count($this->fileQueue)) {
                if ($displayedWaitMessage && $this->finishOnEmptyFolder) {
                    $this->output->writeln(Emoji::wavingHand() . ' Finish watching because of empty folder.');
                    $loop->stop();
                    $this->showStatistics();
                    return;
                }

                if (!$displayedWaitMessage) {
                    $this->output->writeln(Emoji::sleepingFace() . ' Waiting for new files...');
                    $displayedWaitMessage = true;
                }

                return;
            }

            $displayedWaitMessage = false;
            $displayedNewMessage = false;

            foreach ($this->fileQueue as $file) {
                $filesFolder = ArrayHelper::getArray(
                    glob($this->path . DIRECTORY_SEPARATOR . $this->filesPattern)
                );

                $fileQueueCount = count($this->fileQueue);
                $filesFolderCount = count($filesFolder);

                $filesDiff = $filesFolderCount - $fileQueueCount;

                $this->output->write(Emoji::fileFolder() . ' The folder contains currently ' . $filesFolderCount . ' file' . ($fileQueueCount > 1 ? 's' : '') . '. ');

                if ($filesDiff > 0 && !$displayedNewMessage) {
                    $this->output->write(
                        $filesDiff . ' ' . ($filesDiff > 1 ? 'files have' : 'file has') . ' been added previously.'
                    );
                    $displayedNewMessage = true;
                }

                $this->output->writeln('');

                $this->handleFile($file);

                if (true === $this->hasStopped) {
                    break;
                }
            }
        };
    }

    /**
     * @return Closure
     */
    private function handleWatch(): Closure
    {
        return function ($file) {
            $pattern = $this->path . DIRECTORY_SEPARATOR . $this->filesPattern;
            $fileMatchesPattern = fnmatch($pattern, $file);
            $isFolderAllowed = true;

            if (true !== $this->includeSubfolders) {
                $dirname = (string) PathInfo::createFromFile($file)->getDirName();
                $isFolderAllowed = $dirname === $this->path;
            }

            if (!$fileMatchesPattern || !$isFolderAllowed) {
                return;
            }

            $this->addFileToQueue($file);
        };
    }

    private function showStatistics(): void
    {
        try {
            $this->measurement->endAll();
        } catch (TimeException $timeException) {
            return;
        }

        $timeString = 'and unknown time';

        if (null !== $this->unit) {
            $time = $this->unit->getTime();
            $duration = new Duration($time);
            $timeString = $duration->humanize();
        }

        $this->output->writeln(Emoji::crossMark() . '  Stopped watching.');
        $this->output->writeln(Emoji::lightBulb() . ' Ran for ' . $timeString . ' and handled ' . $this->handledFilesCount . ' file' . ($this->handledFilesCount > 1 ? 's' : '') . '.');
    }
}
