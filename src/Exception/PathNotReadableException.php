<?php

/**
 * Folder Watcher
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\FolderWatcher\Exception;

use BitAndBlack\FolderWatcher\Exception;

/**
 * Class PathNotReadableException
 *
 * @package BitAndBlack\FolderWatcher\Exception
 */
class PathNotReadableException extends Exception
{
    /**
     * PathNotReadableException constructor.
     *
     * @param string $path
     */
    public function __construct(string $path)
    {
        parent::__construct('Failed to read from path "' . $path . '"');
    }
}
